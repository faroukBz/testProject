package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@RequestMapping("/api")
@RestController
public class WidgetController {

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Response index() {
        return Response.status(200).entity(new Widget("green", 10, 7)).build();
    }
    @RequestMapping(path = "/lol",method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Response getLol(@QueryParam("query") String query){return Response.status(200).entity(query).build();}
}